package com.benzoate.benzoate;

import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.benzoate.benzoate.BackGroudUtility.DataBaseHelper;


public class CreateChild extends MainActivity {

    DataBaseHelper myDB;
    EditText editTextName, editTextStars, editTextId;
    Button buttonAddChild, buttonConfirm1, buttonConfirm2, buttonConfirm3, buttonCancel;
    Button buttonViewAllChildren;
    Button buttonUpdateChild;
    Button buttonDeleteChild;
    TextView titleName, titleID, titleStars;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_child);
        myDB = new DataBaseHelper(this);
        editTextName = findViewById(R.id.editTextName);
        editTextStars = findViewById(R.id.editTextStars);
        editTextId = findViewById(R.id.editTextID);
        buttonAddChild = findViewById(R.id.button_add_child);
        buttonViewAllChildren = findViewById(R.id.button_view_all_children);
        buttonUpdateChild = findViewById(R.id.button_update_child);
        buttonDeleteChild = findViewById(R.id.button_delete_child);
        buttonConfirm1 = findViewById(R.id.button_confirm1);
        buttonConfirm2 = findViewById(R.id.button_confirm2);
        buttonConfirm3 = findViewById(R.id.button_confirm3);
        buttonCancel = findViewById(R.id.button_cancel);
        titleName = findViewById(R.id.textView_name);
        titleStars = findViewById(R.id.textView_stars);
        titleID = findViewById(R.id.textView_ID);
    }

    public void makeAddAChildFieldsVisible(View view) {
        originalButtonDisappear(view);
        titleName.setVisibility(View.VISIBLE);
        titleStars.setVisibility(View.VISIBLE);
        editTextStars.setVisibility(View.VISIBLE);
        editTextName.setVisibility(View.VISIBLE);
        buttonConfirm1.setVisibility(View.VISIBLE);
        buttonCancel.setVisibility(View.VISIBLE);
    }

    public void makeUpdateAChildFieldsVisible(View view) {
        originalButtonDisappear(view);
        titleID.setVisibility(View.VISIBLE);
        titleName.setVisibility(View.VISIBLE);
        titleStars.setVisibility(View.VISIBLE);
        editTextId.setVisibility(View.VISIBLE);
        editTextStars.setVisibility(View.VISIBLE);
        editTextName.setVisibility(View.VISIBLE);
        buttonConfirm2.setVisibility(View.VISIBLE);
        buttonCancel.setVisibility(View.VISIBLE);
    }

    public void makeDeleteFieldVisible(View view) {
        originalButtonDisappear(view);
        titleID.setVisibility(View.VISIBLE);
        editTextId.setVisibility(View.VISIBLE);
        buttonConfirm3.setVisibility(View.VISIBLE);
        buttonCancel.setVisibility(View.VISIBLE);
    }
    public void originalButtonDisappear (View view){
        buttonViewAllChildren.setVisibility(View.GONE);
        buttonAddChild.setVisibility(View.GONE);
        buttonUpdateChild.setVisibility(View.GONE);
        buttonDeleteChild.setVisibility(View.GONE);
    }
    public void addChild(View view) {

        if ((editTextName.getText().toString().trim().length() == 0) || (editTextStars.getText().toString().trim().length() == 0)) {
            toastMessage(getString(R.string.error_message_need_fill_fields));
        } else {
            cancelButton(view);
            boolean isInserted = myDB.createChild(editTextName.getText().toString(),
                    Integer.parseInt(editTextStars.getText().toString()));

            if (isInserted)
                Toast.makeText(CreateChild.this, "CreateChild added", Toast.LENGTH_LONG).show();
            else
                Toast.makeText(CreateChild.this, "CreateChild not added", Toast.LENGTH_LONG).show();
        }
        editTextName.setText("");
        editTextStars.setText("");
    }


    public void viewAllChildren(View view) {
        Cursor res = myDB.getAllChilds();
        if (res.getCount() == 0) {
            showMessage("Error", getString(R.string.No_activity_create_child));
        }
        StringBuilder buffer = new StringBuilder();
        while (res.moveToNext()) {
            buffer.append("Id : ").append(res.getString(0)).append("\n");
            buffer.append("user_name : ").append(res.getString(1)).append("\n");
            buffer.append("stars : ").append(res.getString(2)).append("\n\n");
        }
        showMessage("Children", buffer.toString());
    }


    public void updateChild(View view) {
        Cursor res = myDB.getAllChilds();
        String idInDatabase = editTextId.getText().toString();
        boolean idPresentInDataBase = false;
        if (res.getCount() == 0) {
            showMessage("Error", getString(R.string.No_activity_create_child));
        }
        while (res.moveToNext()) {
            if (res.getString(0).equals(idInDatabase)) {
                idPresentInDataBase = true;
            }
        }

        if (idPresentInDataBase) {
            if ((editTextName.getText().toString().trim().length() == 0) || (editTextStars.getText().toString().trim().length() == 0)) {
                toastMessage(getString(R.string.error_message_need_fill_fields));
            } else {
                cancelButton(view);
                boolean isUpdate = myDB.updateChild(editTextId.getText().toString(),
                        editTextName.getText().toString(),
                        Integer.parseInt(editTextStars.getText().toString()));
                if (isUpdate)
                    Toast.makeText(CreateChild.this, R.string.created_child, Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(CreateChild.this, R.string.no_child_erased, Toast.LENGTH_LONG).show();
            }
        } else {
            toastMessage(getString(R.string.child_not_exist));

        }
        editTextName.setText("");
        editTextStars.setText("");
        editTextId.setText("");
    }

    public void deleteChild(View view) {
        int deleteRows = myDB.deleteChild(editTextId.getText().toString());
        cancelButton(view);
        if (deleteRows > 0)
            Toast.makeText(CreateChild.this, "CreateChild deleted", Toast.LENGTH_LONG).show();
        else
            Toast.makeText(CreateChild.this, "No activity create child edited", Toast.LENGTH_LONG).show();
        editTextId.setText("");
    }


    public void showMessage(String Title, String Message) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(Title);
        builder.setMessage(Message);
        builder.show();

    }

    public void cancelButton(View view) {

        buttonViewAllChildren.setVisibility(View.VISIBLE);
        buttonAddChild.setVisibility(View.VISIBLE);
        buttonUpdateChild.setVisibility(View.VISIBLE);
        buttonDeleteChild.setVisibility(View.VISIBLE);
        titleID.setVisibility(View.GONE);
        titleName.setVisibility(View.GONE);
        titleStars.setVisibility(View.GONE);
        editTextId.setVisibility(View.GONE);
        editTextStars.setVisibility(View.GONE);
        editTextName.setVisibility(View.GONE);
        buttonConfirm1.setVisibility(View.GONE);
        buttonConfirm2.setVisibility(View.GONE);
        buttonConfirm3.setVisibility(View.GONE);
        buttonCancel.setVisibility(View.GONE);
    }
}
