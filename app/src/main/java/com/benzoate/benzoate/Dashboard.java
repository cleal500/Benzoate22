package com.benzoate.benzoate;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.benzoate.benzoate.BackGroudUtility.MainMenuActivity;
import com.benzoate.benzoate.TestUtility_NonFunctional.JSONParser;
import com.benzoate.benzoate.TestUtility_NonFunctional.Statistics;

public class Dashboard extends MainMenuActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
    }

    public void goToTask(View view) {
        Intent intent = new Intent(this, AddATask.class);
        startActivity(intent);
    }

    public void goToReward(View view) {
        Intent intent = new Intent(this, Reward.class);
        startActivity(intent);
    }

    public void goToViewAllTask(View view) {
        Intent intent = new Intent(this, RVAllTask.class);
        startActivity(intent);
    }

    public void goToSetting(View view) {
        Intent intent = new Intent(this, Setting.class);
        startActivity(intent);
    }

    public void goToInstructions(View view) {
        Intent intent = new Intent(this, Instructions.class);
        startActivity(intent);
    }

    public void goToJSONParser(View view) {
        Intent intent = new Intent(this, JSONParser.class);
        startActivity(intent);
    }

    public void goToStatistics(View view) {
        Intent intent = new Intent(this, Statistics.class);
        startActivity(intent);
    }
}
