package com.benzoate.benzoate;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.TimePicker;

import com.benzoate.benzoate.BackGroudUtility.DataBaseHelper;
import com.benzoate.benzoate.BackGroudUtility.MainMenuActivity;

import java.sql.Timestamp;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static com.benzoate.benzoate.R.id.button_delete_anyway;
import static com.benzoate.benzoate.R.id.button_delete_task;
import static com.benzoate.benzoate.R.id.button_do_not_delete;
import static com.benzoate.benzoate.R.id.button_update;
import static com.benzoate.benzoate.R.id.set_date_created;
import static com.benzoate.benzoate.R.id.set_time_update;
import static com.benzoate.benzoate.R.id.task_description;
import static com.benzoate.benzoate.R.id.textView_confirm_delete;

public class TaskCreated extends MainMenuActivity {
    EditText updateTaskField;
    DataBaseHelper helper = DataBaseHelper.myDb;
    EditText actualFieldTask;
    TextView actualFieldDate, actualFieldTime;
    RatingBar ratingBar;
    String positionCalcul;
    Button deleteAnyway, doNotDelete, buttonUpdate, buttonDelete, buttonSetDate, buttonSetTime;
    TextView confirmDeleteQuestion;
    Long timeDb;
    public Calendar actualDateAndTime;
    public int year, month, day, hours, min;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_created);
        updateTaskField = findViewById(task_description);
        // Get the Intent that started this activity and extract the string
        Intent intent = getIntent();
        positionCalcul = intent.getStringExtra(AddATask.EXTRA_POSITION);

        // Capture the layout's TextView and set the string as its text
        Cursor res = helper.getAllTasks();
        actualFieldTask = findViewById(task_description);
        ratingBar = findViewById(R.id.ratingBar_stars_tasks_actual);
        actualFieldDate = findViewById(R.id.date_field_created);
        actualFieldTime = findViewById(R.id.time_field_created);
        actualDateAndTime = Calendar.getInstance();
        while (res.moveToNext()) {
            if (res.getString(0).equals(positionCalcul)) {
                actualFieldTask.setText(res.getString(1));
                timeDb = res.getLong(2);
                ratingBar.setRating((res.getInt(3)));
            }
        }

        Date date = new Date(timeDb);
        Format formatDate = new SimpleDateFormat("dd/MM/20yy");
        Format formatTime = new SimpleDateFormat("HH:mm");
        year = date.getYear();
        hours = date.getHours();
        min = date.getMinutes();
        month = date.getMonth();
        day = date.getDate();

        actualFieldTime.setText(formatTime.format(date));
        actualFieldDate.setText(formatDate.format(date));
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, Dashboard.class);
        startActivity(intent);
    }

    public void update(View view) {
        String newTask = actualFieldTask.getText().toString();

        Integer newStars = (int) ratingBar.getRating();
        if ((newTask.trim().length() == 0)) {
            toastMessage(getString(R.string.error_message_register_task));
        } else {

            Timestamp newActualTime = new Timestamp(year, month, day, hours, min, 0, 0);

            helper.updateTask(positionCalcul, newTask, newActualTime.getTime(), newStars);
            toastMessage(getString(R.string.task_update));
            Intent intent = new Intent(this, RVAllTask.class);
            startActivity(intent);
        }
    }

    public void deleteTaskConfirmationQuestionAppear(View view) {
        deleteAnyway = findViewById(button_delete_anyway);
        doNotDelete = findViewById(button_do_not_delete);
        confirmDeleteQuestion = findViewById(textView_confirm_delete);
        buttonUpdate = findViewById(button_update);
        buttonDelete = findViewById(button_delete_task);
        buttonSetDate = findViewById(set_date_created);
        buttonSetTime = findViewById(set_time_update);
        buttonUpdate.setVisibility(View.GONE);
        buttonDelete.setVisibility(View.GONE);
        buttonSetDate.setVisibility(View.GONE);
        buttonSetTime.setVisibility(View.GONE);
        deleteAnyway.setVisibility(View.VISIBLE);
        doNotDelete.setVisibility(View.VISIBLE);
        confirmDeleteQuestion.setVisibility(View.VISIBLE);
    }

    public void deleteTask(View view) {
        helper.deleteTask(positionCalcul);
        removeDeleteConfirmationQuestion(view);
        toastMessage(getString(R.string.task_deleted));
        finish();
        Intent intent = new Intent(this, RVAllTask.class);
        startActivity(intent);
    }

    public void removeDeleteConfirmationQuestion(View view) {
        deleteAnyway.setVisibility(View.GONE);
        doNotDelete.setVisibility(View.GONE);
        confirmDeleteQuestion.setVisibility(View.GONE);
        buttonUpdate.setVisibility(View.VISIBLE);
        buttonDelete.setVisibility(View.VISIBLE);
        buttonSetDate.setVisibility(View.VISIBLE);
        buttonSetTime.setVisibility(View.VISIBLE);
    }

    @SuppressWarnings("deprecation")
    public void setDate(View view) {
        showDialog(999);

    }

    @SuppressWarnings("deprecation")
    public void setTime(View view) {
        showDialog(888);

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            return new DatePickerDialog(this,
                    myDateListener, year, month, day);
        } else if (id == 888) {
            return new TimePickerDialog(this, myTimeListener, hours, min, true);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int onDateYear, int onDateMonth, int onDateDay) {
                    year = onDateYear;
                    month = onDateMonth;
                    day = onDateDay;
                    showDate(onDateYear, onDateMonth + 1, onDateDay);
                }
            };
    private TimePickerDialog.OnTimeSetListener myTimeListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            hours = hourOfDay;
            min = minute;
            showTime(hourOfDay, minute);
        }
    };

    private void showDate(int year, int month, int day) {
        actualFieldDate.setText(new StringBuilder().append(day).append("/")
                .append(month).append("/").append(year));
    }

    private void showTime(int hours, int min) {
        String hourAndDateSeparator = ":";
        if (min < 10) {
            hourAndDateSeparator = ":0";
        }
        actualFieldTime.setText(new StringBuilder().append(hours).append(hourAndDateSeparator).append(min));
    }
}
