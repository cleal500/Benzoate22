package com.benzoate.benzoate;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class Reward extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reward);
    }

    public void goBackToMenu(View view) {
        Intent intent = new Intent(this, Dashboard.class);
        startActivity(intent);
    }


}
