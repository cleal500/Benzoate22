package com.benzoate.benzoate;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.benzoate.benzoate.BackGroudUtility.DataBaseHelper;
import com.benzoate.benzoate.TestUtility_NonFunctional.Parent;


public class SignUp extends MainActivity {

    DataBaseHelper helper = new DataBaseHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_singup);
    }

    public void onSignUpClick(View v) {

        if (v.getId() == R.id.button_signup) {
            EditText name = findViewById(R.id.editText_name);
            EditText email = findViewById(R.id.editText_email);
            EditText username = findViewById(R.id.editText_username);
            EditText password1 = findViewById(R.id.editText_password);
            EditText password2 = findViewById(R.id.editText_conf_password);

            String namestr = name.getText().toString();
            String emailstr = email.getText().toString();
            String usernamestr = username.getText().toString();
            String password1str = password1.getText().toString();
            String password2str = password2.getText().toString();

            if ((namestr.trim().length() == 0) || (emailstr.trim().length() == 0) || (usernamestr.trim().length() == 0)
                    || (password1str.trim().length() == 0) || (password2str.trim().length() == 0)) {
                toastMessage("You need to fill all the information needed");
            } else if (!(emailstr.contains("@"))) {
                toastMessage(getString(R.string.need_valid_email));

            } else if (!password1str.equals(password2str)) {
                //pop up message
                Toast.makeText(SignUp.this, "Passwords don't match!", Toast.LENGTH_LONG).show();
            } else { //insert the details in database
                Parent Parent = new Parent();
                Parent.setName(namestr);
                Parent.setEmail(emailstr);
                Parent.setUsername(usernamestr);
                Parent.setPassword(password1str);

                helper.insertParent(Parent);

                Intent i = new Intent(SignUp.this, Dashboard.class);
                startActivity(i);
            }
        }
    }
}
