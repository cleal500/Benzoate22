package com.benzoate.benzoate;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.benzoate.benzoate.BackGroudUtility.MainMenuActivity;

public class Setting extends MainMenuActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
    }

    public void goBackToMenu(View view) {
        Intent intent = new Intent(this, Dashboard.class);
        startActivity(intent);
    }

    public void goToCreateChild(View view) {
        Intent intent = new Intent(this, CreateChild.class);
        startActivity(intent);
    }

    public void goToAbout(View view) {
        Intent intent = new Intent(this, About.class);
        startActivity(intent);
    }
}
