package com.benzoate.benzoate.BackGroudUtility;


import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.benzoate.benzoate.R;
import com.benzoate.benzoate.TaskCreated;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import static com.benzoate.benzoate.AddATask.EXTRA_POSITION;
import static com.benzoate.benzoate.AddATask.EXTRA_TASK;
import static com.benzoate.benzoate.AddATask.EXTRA_TIME;

public class AdapterRYAllTask extends RecyclerView.Adapter<AdapterRYAllTask.MyViewHolder> {

    private DataBaseHelper helper = DataBaseHelper.myDb;
    private Context context;

    private ArrayList<TaskObject> getTask() {

        ArrayList<TaskObject> allTask = new ArrayList<>();

        Cursor res = helper.getAllTasks();
        if (res.getCount() == 0) {
            TaskObject temp = new TaskObject("0", "Erreur Empty list", (long) 0.0);

            allTask.add(temp);

        }
        while (res.moveToNext()) {
            TaskObject temp = new TaskObject(res.getString(0), res.getString(1), res.getLong(2));

            allTask.add(temp);
        }

        Collections.sort(
                allTask,
                (time1, time2) -> time1.compareTo(time2));

        return allTask;
    }


    @Override
    public int getItemCount() {
        return getTask().size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_cell_r_y_application, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        String pair1 = getTask().get(position).getNameOfTask();
        Long time = getTask().get(position).getTimeTask();

        Date date = new Date(time);
        Format format = new SimpleDateFormat("MM dd HH:mm");

        String pair2 = format.format(date);
        String poss = getTask().get(position).getPosition();
        holder.display(pair1, pair2, poss);
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        private final TextView name;
        private final TextView description;

        private String currentFirst;
        private String currentSecond;
        private String currentPoss;


        MyViewHolder(final View itemView) {
            super(itemView);
            context = itemView.getContext();
            //original
            name = itemView.findViewById(R.id.name);
            description = itemView.findViewById(R.id.description);

            itemView.setOnClickListener(view -> {

                if (!currentFirst.equals("Erreur Empty list")) {


                    Intent intent = new Intent(itemView.getContext(), TaskCreated.class);

                    intent.putExtra(EXTRA_TASK, currentFirst);
                    intent.putExtra(EXTRA_TIME, currentSecond);
                    intent.putExtra(EXTRA_POSITION, currentPoss);

                    context.startActivity(intent);
                }
            });
        }

        void display(String first, String second, String poss) {
            currentFirst = first;
            currentSecond = second;
            currentPoss = poss;
            name.setText(first);
            description.setText(second);
        }
    }


}