package com.benzoate.benzoate.BackGroudUtility;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.benzoate.benzoate.TestUtility_NonFunctional.Parent;

public class DataBaseHelper extends SQLiteOpenHelper {

    private static final String TAG = "DataBaseHelper";
    // columns of the parents table
    private static final String TABLE_PARENT = "parents";
    private static final String COLUMN_FAMILY_ID = "_id";
    private static final String COLUMN_PARENTS_NAME = "parent_name";
    private static final String COLUMN_PARENTS_EMAIL = "parent_email";
    private static final String COLUMN_PARENTS_USERNAME = "parent_username";
    private static final String COLUMN_PARENTS_PASSWORDS = "parent_password";
    // columns of the child table
    private static final String TABLE_CHILD = "users";
    private static final String COLUMN_CHILD_ID = "_id";
    private static final String COLUMN_CHILD_NAME = "user_name";
    private static final String COLUMN_CHILD_STARS = "stars";
    // columns of the task table
    private static final String TABLE_TASK = "task";
    private static final String COLUMN_TASK_ID = "_id";
    private static final String COLUMN_TASK_DESCR = "task_description";
    private static final String COLUMN_TASK_DEADLINE = "task_deadline";
    private static final String COLUMN_TASK_ASSIGNED_TO = "task_assigned_to";
    private static final String COLUMN_STARS_VALUE = "stars_value";
    private static final String COLUMN_COMPLETED = "completed";
    private static final String COLUMN_COMPLETED_BY = "completed_by";
    private static final String COLUMN_COMPLETED_AT = "completed_at";
    private static final String COLUMN_APPROVED = "approved";
    private static final String COLUMN_APPROVED_BY = "approved_by";
    private static final String COLUMN_APPROVED_AT = "approved_at";
    private static final String DATABASE_NAME = "benzoate.db";
    private static final int DATABASE_VERSION = 13;
    // SQL statement of the parent table creation
    private static final String SQL_CREATE_TABLE_PARENT = "CREATE TABLE " + TABLE_PARENT + "("
            + COLUMN_FAMILY_ID + " INTEGER PRIMARY KEY, "
            + COLUMN_PARENTS_NAME + " TEXT NOT NULL, "
            + COLUMN_PARENTS_EMAIL + " TEXT NOT NULL,"
            + COLUMN_PARENTS_USERNAME + " TEXT NOT NULL,"
            + COLUMN_PARENTS_PASSWORDS + " TEXT NOT NULL"
            + ");";
    // SQL statement of the child table creation
    private static final String SQL_CREATE_TABLE_CHILD = "CREATE TABLE " + TABLE_CHILD + "("
            + COLUMN_CHILD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_CHILD_NAME + " TEXT NOT NULL, "
            + COLUMN_CHILD_STARS + " INTEGER"
            + ");";
    // SQL statement of the task table creation
    private static final String SQL_CREATE_TABLE_TASK = "CREATE TABLE " + TABLE_TASK + "("
            + COLUMN_TASK_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_TASK_DESCR + " TEXT NOT NULL, "
            + COLUMN_TASK_DEADLINE + " LONG, "
            + COLUMN_STARS_VALUE + " INTEGER, "
            + COLUMN_TASK_ASSIGNED_TO + " TEXT, "
            + COLUMN_COMPLETED + " INTEGER, "
            + COLUMN_COMPLETED_BY + " INTEGER, "
            + COLUMN_COMPLETED_AT + " TEXT, "
            + COLUMN_APPROVED + " INTEGER, "
            + COLUMN_APPROVED_BY + " INTEGER, "
            + COLUMN_APPROVED_AT + " TEXT,"
            + COLUMN_FAMILY_ID + "TEXT"
            + ");";
    public static DataBaseHelper myDb;

    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public DataBaseHelper(Context context, String name, CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    public static void CreateDataBase(Context context) {
        myDb = new DataBaseHelper(context);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(SQL_CREATE_TABLE_PARENT);
        database.execSQL(SQL_CREATE_TABLE_CHILD);
        database.execSQL(SQL_CREATE_TABLE_TASK);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(TAG,
                "Upgrading the database from version " + oldVersion + " to " + newVersion);
        // clear all data
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PARENT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CHILD);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TASK);

        // recreate the tables
        onCreate(db);
    }

    public void insertParent(Parent parent) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        String query = "select * from " + TABLE_PARENT;
        Cursor cursor = db.rawQuery(query, null);
        int count = cursor.getCount();

        contentValues.put(COLUMN_PARENTS_NAME, parent.getName());
        contentValues.put(COLUMN_PARENTS_EMAIL, parent.getEmail());
        contentValues.put(COLUMN_PARENTS_USERNAME, parent.getUsername());
        contentValues.put(COLUMN_PARENTS_PASSWORDS, parent.getPassword());

        db.insert(TABLE_PARENT, null, contentValues);
        db.close();
    }

    public String searchPassword(String username) {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "select parent_username, parent_password from " + TABLE_PARENT;
        Cursor cursor = db.rawQuery(query, null);
        String p_username, p_password;
        p_password = "not found";
        if (cursor.moveToFirst()) {
            do {
                p_username = cursor.getString(0);

                if (p_username.equals(username)) {
                    p_password = cursor.getString(1);
                    break;
                }
            }
            while (cursor.moveToNext());
        }
        return p_password;
    }

    //Childs
    public Boolean createChild(String name, Integer stars) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(DataBaseHelper.COLUMN_CHILD_NAME, name);
        contentValues.put(DataBaseHelper.COLUMN_CHILD_STARS, stars);

        long result = db.insert(TABLE_CHILD, null, contentValues);
        db.close();

        return result != -1;
    }

    public Cursor getAllChilds() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor result = db.rawQuery("Select * from " + TABLE_CHILD, null);
        return result;
    }

    // ôte boolean ou change method
    public boolean updateChild(String id, String name, Integer stars) {
        boolean childExistAlready = true;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DataBaseHelper.COLUMN_CHILD_ID, id);
        contentValues.put(DataBaseHelper.COLUMN_CHILD_NAME, name);
        contentValues.put(DataBaseHelper.COLUMN_CHILD_STARS, stars);
        db.update(TABLE_CHILD, contentValues, "_id = ?", new String[]{id});
        return childExistAlready;

    }

    public int deleteChild(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_CHILD, "_id = ?", new String[]{id});

    }

    //Tasks
    public Boolean createTask(String descr, Long deadline, Integer stars) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(DataBaseHelper.COLUMN_TASK_DESCR, descr);
        contentValues.put(DataBaseHelper.COLUMN_TASK_DEADLINE, deadline);
        contentValues.put(DataBaseHelper.COLUMN_STARS_VALUE, stars);

        long result = db.insert(TABLE_TASK, null, contentValues);
        db.close();

        return result != -1;
    }

    public boolean updateTask(String id, String descr, Long deadline, Integer stars) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DataBaseHelper.COLUMN_TASK_DESCR, descr);
        contentValues.put(DataBaseHelper.COLUMN_TASK_DEADLINE, deadline);
        contentValues.put(DataBaseHelper.COLUMN_STARS_VALUE, stars);
        db.update(TABLE_TASK, contentValues, "_id = ?", new String[]{id});
        return true;

    }

    public Cursor getAllTasks() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("Select * from " + TABLE_TASK, null);
        return res;
    }

    public int deleteTask(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_TASK, "_id = ?", new String[]{id});

    }
}
