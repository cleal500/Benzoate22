package com.benzoate.benzoate.BackGroudUtility;

public class TaskObject implements Comparable<TaskObject> {
    private String position;
    private String nameOfTask;
    private Long timeOfTask;

    TaskObject(String position, String nameOfTask, Long timeOfTask) {
        this.position = position;
        this.nameOfTask = nameOfTask;
        this.timeOfTask = timeOfTask;
    }

    public String getPosition() {
        return position;
    }

    public String getNameOfTask() {
        return nameOfTask;
    }

    public Long getTimeTask() {
        return timeOfTask;
    }

    @Override
    public int compareTo(TaskObject o) {
        return getTimeTask().compareTo(o.getTimeTask());
    }
}
