package com.benzoate.benzoate;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.benzoate.benzoate.BackGroudUtility.AdapterRYAllTask;
import com.benzoate.benzoate.BackGroudUtility.MainMenuActivity;

public class RVAllTask extends MainMenuActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.adapter_r_y_all_task);
        final RecyclerView rv = findViewById(R.id.list);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setAdapter(new AdapterRYAllTask());
    }
}
