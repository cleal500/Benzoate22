package com.benzoate.benzoate;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.benzoate.benzoate.BackGroudUtility.DataBaseHelper;
import com.benzoate.benzoate.BackGroudUtility.MainMenuActivity;

import java.sql.Timestamp;
import java.util.Calendar;

public class AddATask extends MainMenuActivity {

    DataBaseHelper myDB;
    EditText editTextDescr;
    RatingBar ratingBarStars;
    public static final String EXTRA_TASK = "com.example.android.gestionfam5mars.TASK";
    public static final String EXTRA_TIME = "com.example.android.gestionfam5mars.TIME";
    public static final String EXTRA_POSITION = "com.example.android.gestionfam5mars.POSITIONS";


    public Calendar actualDateAndTime;
    public TextView dateView, timeView;
    public int year, month, day, hours, min;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_atask);

        myDB = new DataBaseHelper(this);
        editTextDescr = findViewById(R.id.editTextDescr);
        ratingBarStars = findViewById(R.id.ratingBar_stars_tasks);
        dateView = findViewById(R.id.textView_date_picker);
        timeView = findViewById(R.id.textView_time_picker);
        actualDateAndTime = Calendar.getInstance();
        year = actualDateAndTime.get(Calendar.YEAR);
        hours = actualDateAndTime.get(Calendar.HOUR_OF_DAY);
        min = actualDateAndTime.get(Calendar.MINUTE);

        month = actualDateAndTime.get(Calendar.MONTH);
        day = actualDateAndTime.get(Calendar.DAY_OF_MONTH);
        showDate(year, month + 1, day);
        showTime(hours, min);
    }

    public void registerTask(View view) {

        String message = editTextDescr.getText().toString();

        if ((message.trim().length() == 0)) {
            toastMessage(getString(R.string.error_message_register_task));
            editTextDescr.setText("");
        } else {

            Timestamp actualTime = new Timestamp(year, month, day, hours, min, 0, 0);
            boolean isInserted = myDB.createTask(message, actualTime.getTime(), (int) ratingBarStars.getRating());

            if (isInserted)
                Toast.makeText(AddATask.this,
                        R.string.task_added, Toast.LENGTH_LONG).show();
            else
                Toast.makeText(AddATask.this, R.string.task_not_added, Toast.LENGTH_LONG).show();
            editTextDescr.setText("");
        }

    }

    @SuppressWarnings("deprecation")
    public void setDate(View view) {
        showDialog(999);
    }

    public void setTime(View view) {
        showDialog(888);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        if (id == 999) {
            return new DatePickerDialog(this,
                    myDateListener, year, month, day);
        } else if (id == 888) {
            return new TimePickerDialog(this, myTimeListener, hours, min, true);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int onDateYear, int onDateMonth, int onDateDay) {
                    year = onDateYear;
                    month = onDateMonth;
                    day = onDateDay;
                    showDate(onDateYear, onDateMonth + 1, onDateDay);
                }
            };
    private TimePickerDialog.OnTimeSetListener myTimeListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            hours = hourOfDay;
            min = minute;
            showTime(hourOfDay, minute);
        }
    };

    private void showDate(int year, int month, int day) {
        dateView.setText(new StringBuilder().append(day).append("/")
                .append(month).append("/").append(year));

    }

    private void showTime(int hours, int min) {
        String hourAndDateSeparator = ":";
        if (min < 10) {
            hourAndDateSeparator = ":0";
        }
        timeView.setText(new StringBuilder().append(hours).append(hourAndDateSeparator).append(min));
    }

}
