
package com.benzoate.benzoate.TestUtility_NonFunctional;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class CalculateurEtoiles {

    private int recompensesEnfants;
    private int recompensesParents;

    public static final int TOTAL_TACHES_MIN = 0;
    public static final int TOTAL_TACHES_MAX = 100;
    public static final int TOTAL_ETOILES_MAX = 5000;
    public static final int RECOMPENSE_MAX =  5;
    public static final int RECOMPENSE_MIN =  5;


    protected static org.json.JSONObject resultat;
    HashMap<Integer, Integer> calculRegleHashMap;
    TypeSession typesession;

    public CalculateurEtoiles(TypeSession typeSession) {
        calculeTout(typeSession);
    }
          
    public void  calculeTout( TypeSession session){

        int etoilesGlobal = 0;

        Iterator<Map.Entry<String, Famille>> familleIterator= session.getFamilles();

        while (familleIterator.hasNext()) {
            Map.Entry<String, Famille> next = familleIterator.next();
            Famille famille = next.getValue();

            try {
                int etoilesTotal =  calculerRecompensesParFamille(famille);
                famille.setTotalEtoilesFamille(etoilesTotal);
                etoilesGlobal = etoilesGlobal + etoilesTotal;
                session.setEtoilesTotal(etoilesGlobal);
            } catch (Exception ex) {
            }
       }
       
    }
   
    public int calculerRecompensesParFamille(Famille famille){
   
        int a= calculRecompenseParents(famille.getRecompensesParents(), famille.getTotalTachesParents());
        int b= calculRecompenseEnfants(famille.getRecompensesEnfants(), famille.getTotalTachesEnfants());
        famille.setTotalEtoilesFamille(a+b);
       
        return a+b;
    }

     public int calculRecompenseParents(int recompenses, int totTaches){
	int res;
        res= totTaches*recompenses;
	    res = verificationValeurEtoiles(res);
	return res;
    }
     
    public int calculRecompenseEnfants(int recompenses, int totTaches){
	int res;
         res=  totTaches*recompenses;
	    res = verificationValeurEtoiles(res);
	return res;
    }
     
    
    public int verificationValeurEtoiles(int valeur){
	int res= valeur;
        if (valeur>TOTAL_ETOILES_MAX)
	      res = TOTAL_ETOILES_MAX;
    	return res;
    }




}

