package com.benzoate.benzoate.TestUtility_NonFunctional;


import java.text.SimpleDateFormat;
import java.util.Date;

public class Parent {

    private String name, email, username, password, family_id, date_inscription;


    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return this.email;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return this.username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return this.password;
    }

    public void setFamily_id(String id) {
        family_id = id;
    }

    public String getFamily_id() {
        return family_id;
    }


    public void setDateInscription() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        date_inscription = sdf.format(new Date());

    }

    public String getDateInscription() {
        return date_inscription;
    }

}
