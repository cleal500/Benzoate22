package com.benzoate.benzoate.TestUtility_NonFunctional;


public class MyFamilies {

    String nom_famille;
    int nombre_enfants;
    int nombre_parents;
    int total_taches_enfants;
    int total_taches_parents;
    int total_tasks_today_children;
    int total_tasks_today_parents;
    int recompense_enfants;
    int recompense_parents;
    String date_inscription;


    public MyFamilies(){
        super();
    }

    @Override
    public String toString(){
        String output;
        output = "" + getNom_famille() + " Date Inscription: " + getDate_inscription() + "\n";
        return output;
    }

    public String getNom_famille() {
        return nom_famille;
    }

    public void setNom_famille(String nom_famille) {
        this.nom_famille = nom_famille;
    }

    public int getNombre_enfants() {
        return nombre_enfants;
    }

    public void setNombre_enfants(int nombre_enfants) {
        this.nombre_enfants = nombre_enfants;
    }

    public int getNombre_parents() {
        return nombre_parents;
    }

    public void setNombre_parents(int nombre_parents) {
        this.nombre_parents = nombre_parents;
    }

    public int getTotal_taches_enfants() {
        return total_taches_enfants;
    }

    public void setTotal_taches_enfants(int total_taches_enfants) {
        this.total_taches_enfants = total_taches_enfants;
    }

    public int getTotal_taches_parents() {
        return total_taches_parents;
    }

    public void setTotal_taches_parents(int total_taches_parents) {
        this.total_taches_parents = total_taches_parents;
    }

    public int getTotal_tasks_today_children() {
        return total_tasks_today_children;
    }

    public void setTotal_tasks_today_children(int total_tasks_today_children) {
        this.total_tasks_today_children = total_tasks_today_children;
    }

    public int getTotal_tasks_today_parents() {
        return total_tasks_today_parents;
    }

    public void setTotal_tasks_today_parents(int total_tasks_today_parents) {
        this.total_tasks_today_parents = total_tasks_today_parents;
    }

    public int getRecompense_enfants() {
        return recompense_enfants;
    }

    public void setRecompense_enfants(int recompense_enfants) {
        this.recompense_enfants = recompense_enfants;
    }

    public int getRecompense_parents() {
        return recompense_parents;
    }

    public void setRecompense_parents(int recompense_parents) {
        this.recompense_parents = recompense_parents;
    }

    public String getDate_inscription() {
        return date_inscription;
    }

    public void setDate_inscription(String date_inscription) {
        this.date_inscription = date_inscription;
    }

}
