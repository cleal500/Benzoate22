package com.benzoate.benzoate.TestUtility_NonFunctional;



import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DataBaseHelper2 extends SQLiteOpenHelper {

    public static DataBaseHelper2 myDb2;

    public static void CreateDataBase(Context context) {
        myDb2 =  new DataBaseHelper2 (context);
    }

    private static final String TAG = "DataBaseHelper2";

    // columns of the parents table
    private static final String TABLE_PARENT = "parents" ;
    private static final String COLUMN_FAMILY_ID = "_id";
    private static final String COLUMN_PARENTS_FAMILY_ID_STRING = "family_id";
    private static final String COLUMN_PARENTS_NAME = "parent_name";
    private static final String COLUMN_PARENTS_EMAIL = "parent_email";
    private static final String COLUMN_PARENTS_USERNAME = "parent_username";
    private static final String COLUMN_PARENTS_PASSWORDS = "parent_password";
    private static final String COLUMN_PARENTS_DATE_INSCRIPTION = "date_inscription";

    // columns of the child table
    private static final String TABLE_CHILD = "users";
    private static final String COLUMN_CHILD_ID = "_id";
    private static final String COLUMN_CHILD_FAMILY_ID_STRING = "family_id";
    private static final String COLUMN_CHILD_CHOSEN_ID_STRING = "child_id";
    private static final String COLUMN_CHILD_NAME = "user_name";
    private static final String COLUMN_CHILD_STARS = "stars";


    // columns of the todos table
    private static final String TABLE_TASK = "task";
    private static final String COLUMN_TASK_ID = "_id";
    private static final String COLUMN_TASK_FAMILY_ID_STRING = "family_id";
    private static final String COLUMN_TASK_PERSON_NAME = "person_name";
    private static final String COLUMN_TASK_DESCR = "task_description";
    private static final String COLUMN_TASK_DEADLINE = "task_deadline";
    private static final String COLUMN_TASK_ASSIGNED_TO = "task_assigned_to";
    private static final String COLUMN_STARS_VALUE = "stars_value";
    private static final String COLUMN_COMPLETED = "completed";
    private static final String COLUMN_COMPLETED_BY = "completed_by";
    private static final String COLUMN_COMPLETED_AT = "completed_at";
    private static final String COLUMN_APPROVED = "approved";
    private static final String COLUMN_APPROVED_BY = "approved_by";
    private static final String COLUMN_APPROVED_AT = "approved_at";

    private static final String DATABASE_NAME = "benzoate.db";
    private static final int DATABASE_VERSION = 11;


    // SQL statement of the parents table creation
    private static final String SQL_CREATE_TABLE_PARENT = "CREATE TABLE " + TABLE_PARENT + "("
            + COLUMN_FAMILY_ID + " INTEGER PRIMARY KEY, "
            + COLUMN_PARENTS_FAMILY_ID_STRING + " TEXT DEFAULT NULL,"
            + COLUMN_PARENTS_NAME + " TEXT NOT NULL, "
            + COLUMN_PARENTS_EMAIL + " TEXT NOT NULL,"
            + COLUMN_PARENTS_USERNAME +  " TEXT NOT NULL,"
            + COLUMN_PARENTS_PASSWORDS +  " TEXT NOT NULL,"
            + COLUMN_PARENTS_DATE_INSCRIPTION +  " TEXT DEFAULT NULL"
            + ");";


    // SQL statement of the children table creation
    private static final String SQL_CREATE_TABLE_CHILD = "CREATE TABLE " + TABLE_CHILD + "("
            + COLUMN_CHILD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_CHILD_FAMILY_ID_STRING + " TEXT DEFAULT NULL, "
            + COLUMN_CHILD_CHOSEN_ID_STRING+ " TEXT DEFAULT NULL, "
            + COLUMN_CHILD_NAME + " TEXT NOT NULL, "
            + COLUMN_CHILD_STARS + " INTEGER"
            + ");";

    // SQL statement of the task table creation
    private static final String SQL_CREATE_TABLE_TASK = "CREATE TABLE " + TABLE_TASK + "("
            + COLUMN_TASK_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_TASK_FAMILY_ID_STRING + " TEXT DEFAULT NULL, "
            + COLUMN_TASK_PERSON_NAME + " TEXT DEFAULT NULL, "
            + COLUMN_TASK_DESCR + " TEXT NOT NULL, "
            + COLUMN_TASK_DEADLINE + " TEXT, "
            + COLUMN_STARS_VALUE + " INTEGER, "
            + COLUMN_TASK_ASSIGNED_TO + " TEXT, "
            + COLUMN_COMPLETED + " INTEGER, "
            + COLUMN_COMPLETED_BY + " INTEGER, "
            + COLUMN_COMPLETED_AT + " TEXT, "
            + COLUMN_APPROVED + " INTEGER, "
            + COLUMN_APPROVED_BY + " INTEGER, "
            + COLUMN_APPROVED_AT + " TEXT,"
            + COLUMN_FAMILY_ID + "TEXT"
            + ");";

    public DataBaseHelper2(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(SQL_CREATE_TABLE_PARENT);
        database.execSQL(SQL_CREATE_TABLE_CHILD);
        database.execSQL(SQL_CREATE_TABLE_TASK);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(TAG,
                "Upgrading the database from version " + oldVersion + " to " + newVersion);
        // clear all data
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PARENT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CHILD);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TASK);

        // recreate the tables
        onCreate(db);
    }

    public DataBaseHelper2(Context context, String name, CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }


    public String searchPass(String username){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "select parent_username, parent_password from " + TABLE_PARENT;
        Cursor cursor = db.rawQuery(query, null);
        String p_username, p_password;
        p_password = "not found";
        if (cursor.moveToFirst())
        {
            do{
                p_username = cursor.getString(0);

                if(p_username.equals(username)){
                    p_password = cursor.getString(1);
                    break;
                }
            }
            while (cursor.moveToNext());
        }
        return p_password;
    }

    //Childs

    public static Cursor getAllChilds(){
        SQLiteDatabase db = myDb2.getWritableDatabase();
        Cursor res =  db.rawQuery("Select * from " + TABLE_CHILD, null);
        return res;
    }

    public boolean updateChild(String id, String name, Integer stars ) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DataBaseHelper2.COLUMN_CHILD_ID, id);
        contentValues.put(DataBaseHelper2.COLUMN_CHILD_NAME, name);
        contentValues.put(DataBaseHelper2.COLUMN_CHILD_STARS, stars);
        db.update(TABLE_CHILD, contentValues, "_id = ?" , new String[] {id});
        return true;

    }

    public int deleteChild(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_CHILD, "_id = ?" , new String[] {id});

    }

    public boolean updateTask(String id,String nom, String descr, String deadline, Integer stars ) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DataBaseHelper2.COLUMN_TASK_PERSON_NAME, nom);
        contentValues.put(DataBaseHelper2.COLUMN_TASK_DESCR, descr);
        contentValues.put(DataBaseHelper2.COLUMN_TASK_DEADLINE, deadline);
        contentValues.put(DataBaseHelper2.COLUMN_STARS_VALUE, stars);
        db.update(TABLE_TASK, contentValues, "_id = ?" , new String[] {id});
        return true;

    }

    public static Cursor getAllTasks(){
        SQLiteDatabase db = myDb2.getWritableDatabase();
        Cursor res =  db.rawQuery("Select * from " + TABLE_TASK, null);
        return res;
    }

    public int deleteTask(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_TASK, "_id = ?" , new String[] {id});

    }


    //Ajout des methodes pour tests

    public void insertParent(Parent parent){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        String query = "select * from " + TABLE_PARENT;
        Cursor cursor = db.rawQuery(query, null);
        int count = cursor.getCount();
        contentValues.put(COLUMN_PARENTS_FAMILY_ID_STRING, parent.getFamily_id() );
        contentValues.put(COLUMN_PARENTS_NAME, parent.getName() );
        contentValues.put(COLUMN_PARENTS_EMAIL, parent.getEmail());
        contentValues.put(COLUMN_PARENTS_USERNAME, parent.getUsername());
        contentValues.put(COLUMN_PARENTS_PASSWORDS, parent.getPassword());
        contentValues.put(COLUMN_PARENTS_PASSWORDS, parent.getPassword());
        contentValues.put(COLUMN_PARENTS_DATE_INSCRIPTION, parent.getDateInscription());

        db.insert(TABLE_PARENT, null, contentValues);
        db.close();
    }

    public Boolean createTask(String id_famille, String nom, String descr, String deadline, Integer stars) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        String query = "select * from " + TABLE_TASK;
        Cursor cursor = db.rawQuery(query, null);
        int count = cursor.getCount();

        contentValues.put(DataBaseHelper2.COLUMN_TASK_FAMILY_ID_STRING, id_famille);
        contentValues.put(DataBaseHelper2.COLUMN_TASK_PERSON_NAME, nom);
        contentValues.put(DataBaseHelper2.COLUMN_TASK_DESCR, descr);
        contentValues.put(DataBaseHelper2.COLUMN_TASK_DEADLINE, deadline);
        contentValues.put(DataBaseHelper2.COLUMN_STARS_VALUE, stars);

        long result = db.insert(TABLE_TASK, null, contentValues);
        db.close();

        return result != -1;
    }

    public Boolean createChild(String idFamille, String name, Integer stars) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        String query = "select * from " + TABLE_CHILD;
        Cursor cursor = db.rawQuery(query, null);
        int count = cursor.getCount();

        contentValues.put(DataBaseHelper2.COLUMN_CHILD_FAMILY_ID_STRING, idFamille);
        contentValues.put(DataBaseHelper2.COLUMN_CHILD_NAME, name);
        contentValues.put(DataBaseHelper2.COLUMN_CHILD_STARS, stars);

        long result = db.insert(TABLE_CHILD, null, contentValues);
        db.close();

        return result != -1;
    }

    public static Cursor getAllParents(){
        SQLiteDatabase db = myDb2.getWritableDatabase();
        Cursor res =  db.rawQuery("Select * from " + TABLE_PARENT, null);
        return res;
    }

    public static void eraseDB(){
        SQLiteDatabase db = myDb2.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PARENT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CHILD);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TASK);
    }


}
