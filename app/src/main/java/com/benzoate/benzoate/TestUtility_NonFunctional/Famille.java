package com.benzoate.benzoate.TestUtility_NonFunctional;

import java.util.ArrayList;

public class Famille {

    ValidateurDonnee validateur;

    String nom_famille;
    private int nb_enfants;
    private int nb_parents;
    private int total_taches_enfants;
    private int total_taches_parents;
    private int total_tasks_today_children;
    private int total_tasks_today_parents;
    private int recompense_enfants;
    private int recompense_parents;
    private String date_inscription;

    int total_membres;
    private int total_taches;
    int total_etoiles_famille;

    public Famille(){super();}


    public void createFamille(MyFamilies input) throws ValidationDonneesException {
        validateur = new ValidateurDonnee();
        setNomFamille(input.getNom_famille());
        setDateInscription(input.getDate_inscription());
        setnbEnfants(input.getNombre_enfants());
        setnbParents(input.getNombre_parents());
        setRecompenseEnfants(input.getRecompense_enfants());
        setRecompenseParents(input.getRecompense_parents());
        setTotalTachesAFaireEnfants(input.getTotal_tasks_today_children());
        setTotalTachesAFaireParents(input.getTotal_tasks_today_parents());
        setTotalTachesParents(input.getTotal_taches_parents());
        setTotalTachesEnfants(input.getTotal_taches_enfants());
        setTotalMembres();
        setTotalTaches();
    }

    @Override
    public String toString(){
        return ""+ getNomFamille() + "\n" + getDateInscription() + "\n ";
    }

    void setTotalMembres()throws ValidationDonneesException{
        int resultat = nb_enfants + nb_parents;
        validateur.validateTotalMembres(resultat);
        total_membres = resultat;
    }

    public void setTotalTaches()throws ValidationDonneesException{
        int resultat = total_taches_enfants + total_taches_enfants;
        validateur.validateTotalTaches(resultat);
        total_taches = resultat;
    }

    public void setTotalTaches(int input)throws ValidationDonneesException{
        validateur.validateTotalTaches(input);
        total_taches = input;
    }

    public void setTotalTachesParents(int total) throws ValidationDonneesException{
        validateur.validateTotalTaches(total);
        total_taches_parents = total;
    }

    public void setTotalTachesEnfants(int total) throws ValidationDonneesException{
        validateur.validateTotalTaches(total);
        total_taches_enfants = total;
    }


    public void setRecompensesParents(int total) throws ValidationDonneesException{
        validateur.validateTotalTaches(total);
        total_taches_parents = total;
    }

    public void setRecompensesEnfants(int total) throws ValidationDonneesException{
        validateur.validateTotalEtoiles(total);
        total_taches_enfants = total;
    }



    public void setNomFamille(String nomFamille) throws ValidationDonneesException {
        validateur.validateNomFamille(nomFamille);
        nom_famille=nomFamille;
    }

    public void setnbEnfants(int nbEnfants) throws ValidationDonneesException {
        validateur.validateNbEnfants(nbEnfants);
        nb_enfants=nbEnfants;
    }

    public void setnbParents(int nbParents) throws ValidationDonneesException {
        validateur.validateNbParents(nbParents);
        nb_parents=nbParents;
    }

    void setTotalTachesAFaireEnfants(int totalTaches) throws ValidationDonneesException {
        validateur.validateTotalTaches(totalTaches);
        total_taches=totalTaches;
    }

    void setTotalTachesAFaireParents(int totalTaches) throws ValidationDonneesException {
        validateur.validateTotalTaches(totalTaches);
        total_taches=totalTaches;
    }


    public void setDateInscription(String dateInscription) throws ValidationDonneesException {
        validateur.validateDateInscription(dateInscription);
        date_inscription=dateInscription;
    }


    public void setRecompenseParents(int recomP) throws ValidationDonneesException {
        validateur.validateTotalEtoiles(recomP);
        recompense_parents=recomP;
    }

    public void setRecompenseEnfants(int recomE) throws ValidationDonneesException {
        validateur.validateTotalEtoiles(recomE);
        recompense_enfants=recomE;
    }


    void setTotalEtoilesFamille(int resultat) {
        total_etoiles_famille = resultat;
    }

    public int getTotalMembres() {
        return total_membres ;
    }

    public String getNomFamille(){
        return nom_famille;
    }

    public int getNbEnfants(){
        return nb_enfants;
    }

    public int getNbParents(){
        return nb_parents;
    }

    public int getTotalTachesEnfants(){
        return total_taches_parents;
    }

    public int getTotalTachesParents(){
        return total_taches_enfants;
    }

    public int getTotalTaches(){
        return total_taches_enfants + total_taches_parents;
    }

    public int getTotalTachesAFaireEnfants(){
        return total_tasks_today_children;
    }

    public int getTotalTachesAFaireParents(){
        return total_tasks_today_parents;
    }

    public int getTotalEtoilesFamille() {
        return total_etoiles_famille;
    }

    public String getDateInscription(){
        return date_inscription;
    }

    public int getRecompensesParents(){
        return recompense_parents;
    }

    public int getRecompensesEnfants(){
        return recompense_enfants;
    }


    public  static boolean verificationNomFamille(MyFamilies entree ,int indiceEmp, ArrayList<MyFamilies> liste){
        boolean isValid = true;
        for (int i = 0; i < liste.size()&&isValid; i++) {
            if(entree.getNom_famille().toString().toLowerCase().trim().isEmpty()&& liste.get(i).getNom_famille().toString().toLowerCase().trim().isEmpty()){
                isValid=false;
            }

            if(indiceEmp!=i && entree.getNom_famille().toString().toLowerCase().trim().equals(liste.get(i).getNom_famille().toString().toLowerCase().trim())){
                isValid= false;
            }
        }
        return isValid;
    }
}
