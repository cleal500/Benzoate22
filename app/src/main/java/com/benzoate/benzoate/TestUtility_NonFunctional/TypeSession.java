
package com.benzoate.benzoate.TestUtility_NonFunctional;


import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class TypeSession {

    private String nom_type_session;
    private final ValidateurDonnee validateurDeDonnee;
    private int typeSession;
    private final HashMap<String,Famille> familleList;
    private int total_parents;
    private int total_enfants;
    private int recompense_parents;
    private int recompense_enfants;
    private static int total_etoiles;
    private int total_taches_parents;
    private int total_taches_enfants;
    private int total_tasks_today_parents;
    private int total_tasks_today_enfants;
    private int total_taches;

    public TypeSession(){
        validateurDeDonnee = new ValidateurDonnee();
        familleList= new HashMap<>();
        total_parents = 0;
        total_enfants = 0;
        total_etoiles = 0;
    }

    public void setNomSession(String nom_type_session) throws ValidationDonneesException {
        validateurDeDonnee.validateNomDeSession(nom_type_session);
        this.nom_type_session = nom_type_session;
    }


    public void setEtoilesTotal(int total){
        total_etoiles = total;
    }

    public void setEtoilesTotal(TypeSession mysession) {
        Iterator<Map.Entry<String, Famille>> liste = mysession.getFamilles();
        while (liste.hasNext()){

            Map.Entry<String, Famille> next = liste.next();
            Famille famille = next.getValue();
            total_etoiles = total_etoiles + famille.getTotalEtoilesFamille();
        }

        setEtoilesTotal(total_etoiles);
    }

    public void setTotalEnfants(int total){
        total_enfants = total;
    }

    public void setTotalParents(int total){
        total_parents = total;
    }

    public void setTotalTachesParents(int total){
        total_taches_parents = total;
    }

    public void setTotalTachesEnfants(int total){
        total_taches_enfants = total;
    }


    public int getEtoilesTotal(){
        return total_etoiles;
    }

    public int getTotalEnfants(){
        return total_enfants;
    }

    public int getTotalParents(){
        return total_parents;
    }


    public void setTypeSession(int typeSession) throws ValidationDonneesException {
        validateurDeDonnee.validateTypeSession(typeSession);
        this.typeSession=typeSession;
    }


    public void ajouterFamille(Famille famille) throws ValidationDonneesException  {
        validateurDeDonnee.validerFamilleUnique(familleList,famille);
        setTotalParents(total_parents + famille.getNbParents());
        setTotalEnfants(total_enfants + famille.getNbEnfants());
        familleList.put(famille.getNomFamille(),famille);

    }

    public void setRecompenseEnfants(int recompenseE) throws ValidationDonneesException{
        validateurDeDonnee.validateTotalEtoiles(recompenseE);
        recompense_enfants = recompenseE;
    }

    public void setRecompenseParents(int recompenseP)throws ValidationDonneesException {
        validateurDeDonnee.validateTotalEtoiles(recompenseP);
        recompense_parents = recompenseP;
    }


    public Iterator<Map.Entry<String, Famille>> getFamilles() {
        return familleList.entrySet().iterator();
    }

    public int getTypeSession() {
        return typeSession;
    }

    public String getNomSession() {
        return nom_type_session;
    }


    public int getRecompenseParents(){
        return recompense_parents;
    }


    public int getRecompenseEnfants() {
        return recompense_enfants;
    }


    public int getQuantiteFamilles() {
        return  familleList.size();
    }
}

 
 

    
