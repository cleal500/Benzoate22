package com.benzoate.benzoate.TestUtility_NonFunctional;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import net.sf.json.JSONObject;

import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.HashMap;

public class ValidateurDonnee extends AppCompatActivity {

    static final int SESSION_NUMBER_MIN = 1;
    static final int SESSION_NUMBER_MAX = 2;

    static final int NB_MEMBRES_MIN = 1;
    static final int NB_MEMBRES_MAX = 15;

    static final int NB_ENFANTS_MIN = 0;
    static final int NB_ENFANTS_MAX = 10;

    static final int NB_PARENTS_MIN = 1;
    static final int NB_PARENTS_MAX = 5;

    static final int VALIDER_ETOILES_MIN = 0;
    static final int VALIDER_ETOILES_MAX = 5;

    static final int TOTAL_TACHES_MIN = 0;
    static final int TOTAL_TACHES_MAX = 200;


    static JSONObject resultat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        resultat = new JSONObject();
    }

    public void validateNomDeSession(String nomSession) throws ValidationDonneesException {
        if (nomSession == null || nomSession.isEmpty()) {
            throw new ValidationDonneesException();
        }
    }

    //validType
    public void validateTypeSession(int typeSession) throws ValidationDonneesException {
        if (typeSession > SESSION_NUMBER_MAX || typeSession < SESSION_NUMBER_MIN) {
            throw new ValidationDonneesException();
        }
    }

    public void validateTotalTaches(int totalTaches) throws ValidationDonneesException {
        if (totalTaches > TOTAL_TACHES_MAX || totalTaches < TOTAL_TACHES_MIN) {
            resultat.accumulate("Total de Taches Invalide", true);
            throw new ValidationDonneesException();
        }
    }

    void validerFamilleUnique(HashMap<String, Famille> familleList, Famille famille) throws ValidationDonneesException {
        if (familleList.containsKey(famille.getNomFamille())) {
            throw new ValidationDonneesException();
        }
    }

    public void validateNomFamille(String nom) throws ValidationDonneesException {
        if (nom != null && nom.isEmpty()) {
            throw new ValidationDonneesException();
        }
    }

    public void validateTotalMembres(int totalMembres) throws ValidationDonneesException {
        if (totalMembres < NB_MEMBRES_MIN || totalMembres > NB_MEMBRES_MAX) {
            throw new ValidationDonneesException();
        }
    }

    public void validateNbEnfants(int nbEnfants) throws ValidationDonneesException {
        if (nbEnfants < NB_ENFANTS_MIN || nbEnfants > NB_ENFANTS_MAX) {
            throw new ValidationDonneesException();
        }
    }

    public void validateNbParents(int nbParents) throws ValidationDonneesException {
        if (nbParents < NB_PARENTS_MIN || nbParents > NB_PARENTS_MAX) {
            throw new ValidationDonneesException();
        }
    }

    //validateChargeTravail
    public void validateTotalEtoiles(int nEtoiles) throws ValidationDonneesException {
        if (nEtoiles < VALIDER_ETOILES_MIN || nEtoiles > VALIDER_ETOILES_MAX) {
            throw new ValidationDonneesException();
        }
    }

    //validateDateRevision
    public void validateDateInscription(String dateInscription) throws ValidationDonneesException {
        try {
            DateTimeFormatter timeFormatter = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                timeFormatter = DateTimeFormatter.ISO_DATE;
            }
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                TemporalAccessor accessor = timeFormatter.parse(dateInscription);
            }

        } catch (Exception ex) {
            throw new ValidationDonneesException();
        }
        if (dateInscription.matches("%tF"))
            throw new ValidationDonneesException();
    }
}
