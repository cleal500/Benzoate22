package com.benzoate.benzoate.TestUtility_NonFunctional;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.JsonWriter;
import android.widget.TextView;

import com.benzoate.benzoate.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;


public class JSONParser extends AppCompatActivity {

    TextView familyTextView;
    public static ArrayList<MyFamilies> familyData;
    public static ArrayList<Famille> validatedFamilies;
    public ValidateurDonnee valider;
    public TypeSession session;
    public String detailFamilles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jsonparser);

        familyTextView = findViewById(R.id.textView1);
        detailFamilles = "\n DETAIL FAMILLES \n ";
        familyData = loadJSONFromAsset("entree.json");
        TypeSession typeUsager = new TypeSession();
        validatedFamilies = loadValidFamilies(familyData, typeUsager);
        int typeSession = session.getTypeSession();
        CalculateurEtoiles calculateur = new CalculateurEtoiles(typeUsager);
        try {
            typeUsager.setTypeSession(session.getTypeSession());
            typeUsager.setNomSession(session.getNomSession());
            CalculateurStatistiques.setSession(typeUsager);
            JSONObject resultat = preparerSortie(typeUsager);
            typeUsager.setEtoilesTotal(typeUsager);
            printJSON(resultat);
            printSession(typeUsager);
        } catch (JSONException e) {
            familyTextView.setText(familyTextView.getText() + "onCreate JSON Exception");
        } catch (ValidationDonneesException e) {
            familyTextView.setText(familyTextView.getText() + "onCreate Exception TypeSession Validation ");
        }
    }


    public ArrayList<MyFamilies> loadJSONFromAsset(String JSONfileName) {
        valider = new ValidateurDonnee();
        session = new TypeSession();
        ArrayList<MyFamilies> famList = new ArrayList<>();
        String json = null;
        try {
            InputStream is = getAssets().open(JSONfileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            familyTextView.setText(familyTextView.getText() + "IOException");
            return null;
        }
        try {
            JSONObject obj = new JSONObject(json);
            String nomSession = obj.getString("nom_type_session");
            valider.validateNomDeSession(nomSession);
            int typeSession = obj.getInt("type_session");
            valider.validateTypeSession(typeSession);
            session.setNomSession(nomSession);
            session.setTypeSession(typeSession);
            JSONArray m_jArry = obj.getJSONArray("familles");
            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);
                MyFamilies family = new MyFamilies();
                family.setNom_famille(jo_inside.getString("nom_famille"));
                family.setNombre_parents(jo_inside.getInt("nombre_parents"));
                family.setNombre_enfants(jo_inside.getInt("nombre_enfants"));
                family.setTotal_taches_enfants(jo_inside.getInt("total_taches_enfants"));
                family.setTotal_taches_parents(jo_inside.getInt("total_taches_parents"));
                family.setTotal_tasks_today_children(jo_inside.getInt("total_tasks_today_children"));
                family.setTotal_tasks_today_parents(jo_inside.getInt("total_tasks_today_parents"));
                family.setRecompense_enfants(jo_inside.getInt("recompense_enfants"));
                family.setRecompense_parents(jo_inside.getInt("recompense_parents"));
                family.setDate_inscription(jo_inside.getString("date_inscription"));
                famList.add(family);
            }


        } catch (JSONException e) {
            familyTextView.setText(familyTextView.getText() + "JSONException");

        } catch (ValidationDonneesException e) {
            familyTextView.setText(familyTextView.getText() + "ValidationDonneesException");

        } catch (Exception e) {
            familyTextView.setText(familyTextView.getText() + "Exception");
        }

        return famList;
    }

    public ArrayList<Famille> loadValidFamilies(ArrayList<MyFamilies> input, TypeSession session){
        ArrayList<Famille> output = new ArrayList<Famille>();
        try{
            for (int i=0; i< input.size(); i++){
                Famille f = new Famille();
                f.createFamille(input.get(i));
                Famille.verificationNomFamille(input.get(i), i, input);
                session.ajouterFamille(f);
                output.add(f);
            }
        } catch (Exception e) {
            familyTextView.setText(familyTextView.getText() + "LOAD FAMILIES Exception");
        }
        return output;
    }


    public JSONObject preparerSortie(TypeSession typeUsager) throws JSONException {
        Iterator<Map.Entry<String, Famille>> famillesIterator= typeUsager.getFamilles();
        JSONObject resultat = new JSONObject();
        JSONArray jsonArray = new JSONArray();

        while (famillesIterator.hasNext()) {
            Map.Entry<String, Famille> next = famillesIterator.next();
            Famille famille = next.getValue();
            JSONObject jsonObj = new JSONObject();
            jsonObj.accumulate("nom_famille", famille.getNomFamille());
            jsonObj.accumulate("total_etoiles_famille", famille.getTotalEtoilesFamille());
            detailFamilles = detailFamilles + "\n Nom Famille: " + famille.getNomFamille();
            detailFamilles = detailFamilles + "\n Total Etoiles Famille: " + famille.getTotalEtoilesFamille();
            jsonArray.put(jsonObj);
        }

        try {

            resultat.accumulate("total_parents",typeUsager.getTotalParents());
            resultat.accumulate("total_enfants",typeUsager.getTotalEnfants());
            resultat.accumulate("total_etoiles",typeUsager.getEtoilesTotal());
            resultat.accumulate("familles", jsonArray);

        } catch (JSONException e) {
            familyTextView.setText(familyTextView.getText() + "preparerSortie JSON Exception");
        }

        return resultat;
    }


    void printSession(TypeSession mySession){
        familyTextView.setText(familyTextView.getText() + "\n" + detailFamilles);
    }


    void printJSON(JSONObject resultat){
        String fileContents = resultat.toString();
        writeOutput("sortie", fileContents);
    }


    public void writeOutput(String filename, String input){
        try {
            FileOutputStream fos = openFileOutput(filename, Context.MODE_PRIVATE);
            fos.write(input.getBytes());
            fos.close();
        } catch (FileNotFoundException e) {

        } catch (IOException e) {
        }
    }

    public void writeJsonStream(OutputStream out, TypeSession mySession) throws IOException {
        JsonWriter writer = new JsonWriter(new OutputStreamWriter(out, "UTF-8"));
        writer.setIndent("  ");
        writeJSON(writer, mySession);
        writer.close();
    }

    public void writeJSON(JsonWriter writer, TypeSession mySession) throws IOException {
        writer.beginObject();
        writer.name("total_familles").value(mySession.getQuantiteFamilles());
        writer.name("total_parents").value(mySession.getTotalParents());
        writer.name("total_enfants").value(mySession.getTotalEnfants());
        writer.name("familles");
        writeSortie(writer, mySession);
        writer.endObject();
    }


    public void writeSortie(JsonWriter writer,TypeSession mySession) throws IOException {
        writer.beginArray();
        for (Famille input : validatedFamilies) {
            writeFamille(writer, input);
        }
        writer.endArray();
    }


    public void writeFamille(JsonWriter writer, Famille uneFamille) throws IOException {
        writer.beginObject();
        writer.name("nom_famille").value(uneFamille.getNomFamille());
        writer.name("total_etoiles_famille").value(uneFamille.getTotalEtoilesFamille());
        writer.endObject();
    }

}
