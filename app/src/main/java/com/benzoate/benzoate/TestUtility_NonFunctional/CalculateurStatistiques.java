package com.benzoate.benzoate.TestUtility_NonFunctional;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import java.util.Calendar;

public class CalculateurStatistiques {
    public static final int TOTAL_TACHES_MIN = 0;
    public static final int TOTAL_TACHES_MAX = 200;
    public static final int FAMILLE_MEMBRES_MAX = 15;
    public static final int ENFANTS_MAX = 10;
    public static final int TOTAL_ETOILES_MAX = 500;
    public static final int TOTAL_ETOILES_MIN = 0;

    static final String MSG_STATS_TOTAL_TACHES_MAX = "Total de taches maximale soumise pour une famille: \n";
    static final String MSG_STATS_VAL_ETOILES_MAX = "Valeur budget par famille maximale: \n";
    static final String MSG_TACHES_MOINS_1 = "Familles sans aucune tache: \n";
    static final String MSG_TACHES_ENTRE_50_100 = "Familles avec total des taches entre 50 et 100: \n";
    static final String MSG_TACHES_SUP_100 = "Familles avec total des taches superieur a 100: \n";

    static final String TACHES_ENTRE_50_100 = "Familles avec taches entre 50 & 100 :\n";
    static final String TACHES_MOINS_1 = "Familles avec  moins de 1 tache: \n";
    static final String TACHES_SUP_100 = "Familles avec plus de 100 taches: \n";


    public static final String MSG_INSCRIPTION = "PLus de xix mois avec nous";
    public static final String MSG_ETOILES_TOTAL_MAX = "est la famille avec un total d'etoiles supÃ©rieure Ã  la limite persmis 500000";
    public static final String MSG_TOTAL_TACHES_MIN =" est la famille qui n'a aucune tache";
    public static final String MSG_TOTAL_TACHES_MAX ="Vous avez le maximum des taches possibles.";
    public static final String MSG_TOTAL_ETOILES = "Le total d'etoiles est pres de la limite";
    public static final String MSG_FAMILLE_MEMBRES_MAX = " a le total de membres depasse la limite";
    public static final String MSG_ENFANTS_MAX = " a le total d'enfants depasse la limite";
    public static final String MSG_TOTAL_ETOILES_MIN = "  a le total de etoiles au minimun";

    static int cptMessagesInscription=0;
    static int cptMessagesEtoilesMin=0;
    static int cptMessagesMaxEtoiles=0;
    static int cptMessagesEnfantsMax=0;
    static int cptMessagesMembresMax=0;
    static int cptMessagesTachesMax=0;


    private  ArrayList<Famille> resultat;
    private static TypeSession session;
    public String output;

    public CalculateurStatistiques(ArrayList<Famille> resultat) {
        resultat = new ArrayList<>();
        this.resultat = resultat;
        calculate();

        }

    private void calculate() {
        calculerFamillesSansTaches();
        calculerTachesEntre50et100();
        calculerTachesSup100();
    }

    public static void setSession(TypeSession input){
         session = new TypeSession();
         session = input;
    }


    public void calculerFamillesSansTaches() {
        int taches_zero =0;
        Iterator<Map.Entry<String, Famille>> familleIterator= session.getFamilles();

        while (familleIterator.hasNext()) {
            Map.Entry<String, Famille> next = familleIterator.next();
            Famille famille = next.getValue();
            if (famille.getTotalTaches()<1) {
                taches_zero++;
            }
        }
        output = TACHES_MOINS_1 + taches_zero + "\n";
    }

    private void calculerTachesEntre50et100() {
        int taches_entre_50_100 = 0;

        Iterator<Map.Entry<String, Famille>> familleIterator= session.getFamilles();

        while (familleIterator.hasNext()) {
            Map.Entry<String, Famille> next = familleIterator.next();
            Famille famille = next.getValue();
            if (famille.getTotalTaches()>50 &&famille.getTotalTaches()<100) {
                taches_entre_50_100=taches_entre_50_100+1;
            }
        }
        output = output + TACHES_ENTRE_50_100 + taches_entre_50_100 + "\n";

    }

    private void calculerTachesSup100() {
        int taches_sup_100 = 0;
        Iterator<Map.Entry<String, Famille>> familleIterator= session.getFamilles();

        while (familleIterator.hasNext()) {
            Map.Entry<String, Famille> next = familleIterator.next();
            Famille famillle = next.getValue();
            if (famillle.getTotalTaches()>100) {
                taches_sup_100=taches_sup_100+1;
            }
        }
        output = output + TACHES_SUP_100 + taches_sup_100 +"\n";
    }


    public static String generateMessageMaxEtoiles(int total_etoiles, String nom){
        String message="";
        if (total_etoiles > TOTAL_ETOILES_MAX ){
            cptMessagesMaxEtoiles ++;
            message= nom + MSG_TOTAL_ETOILES + "\n";
        }
        return message;
    }


    protected static String messageTachesMax(int totalTaches, String nom) {
        String message="";
        if(totalTaches > TOTAL_TACHES_MAX ){
            cptMessagesTachesMax++;
            message= nom + MSG_TOTAL_TACHES_MAX + "\n";
        }
        return message;
    }


    public static String messageMembresMax(int membres) {
        String message= "";
        if (membres> FAMILLE_MEMBRES_MAX ){
            cptMessagesMembresMax ++;
            message =  MSG_FAMILLE_MEMBRES_MAX + "\n";
        }
        return message;
    }

    public static String messageEnfantsMax(int enfants) {
        String message="";
        if (enfants>ENFANTS_MAX ){
            cptMessagesEnfantsMax++;
            message=  MSG_ENFANTS_MAX+ "\n";
        }
        return message;
    }

    public static String messageEtoilesTotalMin(int etoilesTotal) {
        String message="";
        if(etoilesTotal < TOTAL_ETOILES_MIN +1 ){
            cptMessagesEtoilesMin ++;
            message =  MSG_TOTAL_ETOILES_MIN +"\n";
        }
        return message;
    }

    protected static String messageDateInscription(String date_inscription) {
        String message="";
        if (comparerDatesInscription(date_inscription)){
            cptMessagesInscription++;
            message= MSG_INSCRIPTION + "\n";
        }
        return message;
    }

    public static String AjoutMessagesSpecialesFamille(Famille famille){
        String message= messageDateInscription(famille.getDateInscription());
        message = message + messageEtoilesTotalMin(famille.getTotalTaches());
        return message;
    }


    protected static String ajoutMessagesTypeSession(Famille famille){
        String output;
        output = generateMessageMaxEtoiles(famille.getTotalEtoilesFamille(), famille.getNomFamille());
        output = output + messageMembresMax(session.getRecompenseParents());
        output = output + messageEnfantsMax(session.getRecompenseEnfants());
        return output;
    }


    public static boolean comparerDatesInscription(String date_inscription){
        boolean comparaison = false;
        Calendar datePresent =  getDatePresent();
        Calendar dateMinimal = getDateMinimal(datePresent);

        try {
            Calendar dateRevision  = getDateInscription(date_inscription);
            if(dateRevision.before(dateMinimal)){
                comparaison = true;
            }
        }catch (Exception e) {
            //Aucun message pour l'instant s`il n y a pas une date date_d'inscription
        }
        return comparaison;
    }

    protected static Calendar getDatePresent(){
        Calendar datePresent =  Calendar.getInstance();
        datePresent.set(datePresent.get(Calendar.YEAR),datePresent.get(Calendar.MONTH),datePresent.get(Calendar.DAY_OF_MONTH));
        return datePresent;
    }

    protected static Calendar getDateMinimal(Calendar datePresent){
        Calendar dateMinimal = Calendar.getInstance();
        dateMinimal = revisionMoisMinimal(datePresent);
        return dateMinimal;
    }

    public static Calendar getDateInscription(String date){
        Calendar dateRevision= Calendar.getInstance();
        int mois = Integer.parseInt(date.substring(5,7));
        int jour = Integer.parseInt(date.substring(8,date.length()));
        int anne = Integer.parseInt(date.substring(0,4));
        dateRevision.set(anne, mois, jour);
        return dateRevision;
    }

    protected static Calendar revisionMoisMinimal(Calendar datePresent){
        int annee = datePresent.get(Calendar.YEAR);
        int mois = datePresent.get(Calendar.MONTH);
        if(mois <= 5){
            mois = mois + 6; annee = annee - 1;
        }else {
            mois = mois - 6;
        }
        Calendar dateMinimal = Calendar.getInstance();
        dateMinimal.set(annee,mois+1,datePresent.get(Calendar.DAY_OF_MONTH));
        return dateMinimal;
    }


}
