package com.benzoate.benzoate.TestUtility_NonFunctional;


import android.content.Context;
import android.database.Cursor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class CreateFile {

    public String nom_famille;
    public String date_inscription;
    public int nombre_parents;
    public int nombre_enfants;
    public int total_taches_enfants;
    public int total_taches_parents;
    public int total_tasks_today_children;
    public int total_tasks_today_parents;
    public int recompense_enfants;
    public int recompense_parents;
    public String acteur;
    public static ArrayList<Famille> familleList;
    public static Famille fam;


    public static void createFalseFamilies(Context context) {

        Parent a = new Parent();
        a.setName("Loco");
        a.setUsername("loco");
        a.setEmail("loco@gmail.com");
        a.setPassword("loco");
        a.setDateInscription();
        a.setFamily_id("family_id_1");
        DataBaseHelper2.myDb2.insertParent(a);

        Parent b = new Parent();
        b.setName("Amor");
        b.setUsername("amor");
        b.setEmail("amor@gmail.com");
        b.setPassword("amor");
        b.setFamily_id("family_id_2");
        DataBaseHelper2.myDb2.insertParent(b);


        DataBaseHelper2.myDb2.createChild("family_id_1","lola", 5);
        DataBaseHelper2.myDb2.createTask("family_id_1", "lola", "task1","2018-04-22", 5);

        DataBaseHelper2.myDb2.createChild("family_id_2","beto", 5);
        DataBaseHelper2.myDb2.createTask("family_id_2", "beto", "task2","2018-04-23", 5);


        Parent c = new Parent();
        c.setName("Amor");
        c.setUsername("amor");
        c.setEmail("amor@gmail.com");
        c.setPassword("amor");
        c.setDateInscription();
        c.setFamily_id("family_id_2");
        DataBaseHelper2.myDb2.insertParent(c);


        Parent d = new Parent();
        d.setName("Gaia");
        d.setUsername("gaia");
        d.setEmail("gaia@gmail.com");
        d.setPassword("gaia");
        d.setDateInscription();
        d.setFamily_id("family_id_3");
        DataBaseHelper2.myDb2.insertParent(d);

        DataBaseHelper2.myDb2.createChild("family_id_3","bombon", 5);
        DataBaseHelper2.myDb2.createTask("family_id_3", "bombon",  "task1","2018-05-23", 5);

    }



    public static String createDataFile() {
        familleList = new ArrayList<Famille>();
        fam = new Famille();
        generateArrayList();

        JSONObject input = getJSONValues();
        String output = input.toString();
        return output;
    }





    public static void generateArrayList() {


        for(Famille input: familleList) {

        }

    }

    public void readDB(){
        nombre_enfants=0;
        nombre_parents=0;
        total_taches_enfants=0;
        total_taches_parents=0;
        total_tasks_today_children=0;
        total_tasks_today_parents=0;

        String new_id;
        String nomEnfantFait="";
        String nomParentFait="";
        String nomEnfant;
        String nomParent;


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String dateTask = sdf.format(new Date());

        Cursor cursor = DataBaseHelper2.getAllParents();

        while (cursor.moveToNext()) {
            date_inscription = cursor.getString(cursor.getColumnIndex("date_inscription")).trim();
            nom_famille = cursor.getString(cursor.getColumnIndex("family_id")).trim().toLowerCase();
            new_id = nom_famille;

            nombre_enfants = countChildren(new_id);
            nombre_parents = countParents(new_id);

            for (int i=0; i<nombre_enfants; i++){
                nomEnfant = getNomEnfant(new_id, nomEnfantFait);
                nomEnfantFait = nomEnfantFait + nomEnfant;
                if (!nomEnfant.equals(null)) {
                    total_taches_enfants =+ countTachesEnfants(new_id, nomEnfantFait);
                    total_tasks_today_children += countTasksTodayChildren(new_id, nomEnfantFait, dateTask);
                    recompense_enfants = countRecompenseEnfants(new_id, nomEnfantFait);
                }
            }

            for (int i=0; i<nombre_parents; i++){
                nomParent = getNomParent(new_id, nomEnfantFait);
                nomParentFait = nomParentFait + nomParent;
                if (!nomParent.equals(null)) {
                    total_taches_parents =+ countTachesParents(new_id, nomParentFait);
                    total_tasks_today_parents += countTasksTodayParents(new_id, nomParentFait, dateTask);
                    recompense_parents= countRecompenseParents(new_id, nomParentFait);
                }
            }

            try {
                fam.setTotalTachesEnfants(total_taches_enfants);
                fam.setTotalTachesParents(total_taches_parents);
                fam.setTotalTaches(total_taches_parents + total_taches_enfants);
                fam.setTotalTachesEnfants(total_taches_enfants);
                fam.setTotalTachesParents(total_taches_parents);
                fam.setnbParents(nombre_parents);
                fam.setnbEnfants(nombre_enfants);
                fam.setDateInscription(date_inscription);
                fam.setRecompenseEnfants(recompense_enfants);
                fam.setnbParents(recompense_parents);
                fam.setNomFamille(nom_famille);

                familleList.add(fam);

            } catch (ValidationDonneesException e) {
                e.printStackTrace();
            }
        }
    }


    public int countRecompenseEnfants(String new_id, String name){
        int resultat =0;
        int recompense = 0;
        int total = 0;
        int compteur=0;

        Cursor cursor = DataBaseHelper2.getAllTasks();
        while (cursor.moveToNext()) {
            acteur = cursor.getString(cursor.getColumnIndex("user_name")).trim();
            nom_famille = cursor.getString(cursor.getColumnIndex("family_id")).trim();
            recompense = Integer.getInteger(cursor.getString(cursor.getColumnIndex("stars_value")).trim());

            if (nom_famille.toLowerCase().equals(new_id.toLowerCase())
                    && acteur.toLowerCase().equals(name.toLowerCase())){

                compteur++;
                total = total + recompense;
            }

        }

        if (compteur>0) {
            resultat = total / compteur;
        }

        return resultat;
    }


    public int countRecompenseParents(String new_id, String name){
        int resultat =0;
        int recompense = 0;
        int total = 0;
        int compteur=0;

        Cursor cursor = DataBaseHelper2.getAllTasks();
        while (cursor.moveToNext()) {
            acteur = cursor.getString(cursor.getColumnIndex("user_name")).trim();
            nom_famille = cursor.getString(cursor.getColumnIndex("family_id")).trim();
            recompense = Integer.getInteger(cursor.getString(cursor.getColumnIndex("stars_value")).trim());

            if (nom_famille.toLowerCase().equals(new_id.toLowerCase())
                    && acteur.toLowerCase().equals(name.toLowerCase())){

                compteur++;
                total = total + recompense;
            }

        }

        if (compteur>0) {
            resultat = total / compteur;
        }

        return resultat;
    }

    private int countTasksTodayChildren(String new_id, String name, String date) {
        int resultat = 0;
        String dateComparer;
        Cursor cursor = DataBaseHelper2.getAllTasks();
        while (cursor.moveToNext()) {
            acteur = cursor.getString(cursor.getColumnIndex("user_name")).trim();
            nom_famille = cursor.getString(cursor.getColumnIndex("family_id")).trim();
            dateComparer = cursor.getString(cursor.getColumnIndex("task_deadline")).trim();

            if (nom_famille.toLowerCase().equals(new_id.trim().toLowerCase())
                    && name.trim().toLowerCase().equals(acteur.toLowerCase())
                    && date.trim().equals(dateComparer))
            {

                resultat++;
            }

        }
        return resultat;
    }



    private int countTasksTodayParents(String new_id, String nom, String date) {
        int resultat = 0;
        String dateComparer;
        Cursor cursor = DataBaseHelper2.getAllTasks();

        while (cursor.moveToNext()) {
            acteur = cursor.getString(cursor.getColumnIndex("user_name")).trim();
            nom_famille = cursor.getString(cursor.getColumnIndex("family_id")).trim();
            dateComparer = cursor.getString(cursor.getColumnIndex("task_deadline")).trim();

            if (nom_famille.toLowerCase().equals(new_id.trim().toLowerCase())
                    && nom.trim().toLowerCase().equals(acteur.toLowerCase())
                    && date.trim().equals(dateComparer))
            {

                resultat++;
            }

        }
        return resultat;
    }

    private String getNomEnfant(String new_id, String nom) {
        String resultat = null;
        Cursor cursor = DataBaseHelper2.getAllChilds();
        while (cursor.moveToNext()) {

            acteur = cursor.getString(cursor.getColumnIndex("user_name")).trim().toLowerCase();
            nom_famille = cursor.getString(cursor.getColumnIndex("family_id")).trim().toLowerCase();

            if (nom_famille.equals(new_id.trim().toLowerCase()) && nom.equals("")) {
                resultat = acteur;
            } else if (nom_famille.equals(new_id)
                    && !nom.trim().toLowerCase().contains(acteur)) {

                resultat = acteur;
            }
        }

        return resultat;
    }

    private String getNomParent(String new_id, String nom) {
        String resultat = null;

        Cursor cursor = DataBaseHelper2.getAllParents();
        while (cursor.moveToNext()) {

            acteur = cursor.getString(cursor.getColumnIndex("user_name")).trim().toLowerCase();
            nom_famille = cursor.getString(cursor.getColumnIndex("family_id")).trim().toLowerCase();

            if (nom_famille.equals(new_id.trim().toLowerCase()) && nom.equals("")) {
                resultat = acteur;
            } else if (nom_famille.equals(new_id.trim().toLowerCase())
                    && !nom.trim().toLowerCase().contains(acteur)) {

                resultat = acteur;
            }
        }

        return resultat;
    }


    private int countTachesEnfants(String new_id, String name) {
        int resultat = 0;
        Cursor cursor = DataBaseHelper2.getAllTasks();
        while (cursor.moveToNext()) {
            acteur = cursor.getString(cursor.getColumnIndex("user_name")).trim();
            nom_famille = cursor.getString(cursor.getColumnIndex("family_id")).trim();

            if (nom_famille.toLowerCase().equals(new_id.toLowerCase())
                    && acteur.toLowerCase().equals(name.toLowerCase())){

                resultat++;
            }

        }
        return resultat;
    }


    private int countTachesParents (String new_id, String name) {
        int resultat = 0;
        Cursor cursor = DataBaseHelper2.getAllTasks();
        while (cursor.moveToNext()) {
            acteur = cursor.getString(cursor.getColumnIndex("user_name")).trim();
            nom_famille = cursor.getString(cursor.getColumnIndex("family_id")).trim();

            if (nom_famille.toLowerCase().equals(new_id.toLowerCase())
                    && acteur.toLowerCase().equals(name.toLowerCase())){

                resultat++;
            }

        }
        return resultat;
    }


    public int countParents (String familyId) {
        int resultat = 0;
        Cursor cursor = DataBaseHelper2.getAllParents();
        while (cursor.moveToNext()) {

            nom_famille = cursor.getString(cursor.getColumnIndex("family_id")).trim();
            if (nom_famille.toLowerCase().equals(familyId.toLowerCase())){
                resultat++;
            }

        }
        return resultat;
    }


    public int countChildren(String nomFamille) {
        int resultat = 0;
        Cursor cursor = DataBaseHelper2.getAllChilds();
        while (cursor.moveToNext()) {
            nom_famille = cursor.getString(cursor.getColumnIndex("family_id")).trim();

            if (nom_famille.toLowerCase().equals(nomFamille.toLowerCase())){
                resultat++;
            }
        }
        return resultat;
    }


    public static JSONObject getJSONValues() {
        JSONObject resultat = new JSONObject();
        JSONArray listeFam = new JSONArray();
        JSONObject values = new JSONObject();
        try{

            resultat.accumulate("nom_type_session","Parents");
            resultat.accumulate("type_session",1);


            for (Famille input : familleList) {

                values.accumulate("nom_famille", input.getNomFamille());
                values.accumulate("date_inscription;", input.getDateInscription());
                values.accumulate("nombre_parents", input.getNbParents());
                values.accumulate("nombre_enfants", input.getNbEnfants());
                values.accumulate("total_taches_enfants", input.getTotalTachesEnfants());
                values.accumulate("total_taches_parents", input.getTotalTachesParents());
                values.accumulate("total_tasks_today_children", input.getTotalTachesAFaireEnfants());
                values.accumulate("total_tasks_today_parents", input.getTotalTachesAFaireParents());
                values.accumulate("recompense_enfants", input.getRecompensesEnfants());
                values.accumulate("recompense_parents", input.getRecompensesParents());

            }

            listeFam.put(values);

            resultat.accumulate("familles", listeFam);


        } catch (JSONException e) {}


        return resultat;
    }


}

