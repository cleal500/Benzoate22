package com.benzoate.benzoate.TestUtility_NonFunctional;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class MappeurDonneesJSON {

        static final String MSG_NOMBRE_ENFANTS = "Le nombre de enfants doit etre entre 0 et 5";
        static final String MSG_NOMBRE_PARENTS = "Le nombre de parents doit etre entre 1 et 4";
        static final String MSG_NOMBRE_TOTAL_TACHES = "La total des taches ne peut pas etre superieure a  2000";
        static final String MSG_NOMBRE_FAMILLES_MAXIMUM = "Le nombre des familles ne doit pas depasser 10";
        static final String MSG_NOMBRE_FAMILLES_MINIMUM = "Une session doit avoir au moins une famille";
        static final String MSG_TYPE_SESSION = "Le type de session doit prendre la valeur 1 ou 2";
        static final String MSG_FORMAT_DATE = "Le format des dates ISO 8601 n'est pas respecte";
        static final String MSG_NOM_SESSION_MANQUANTE = "La propriete nom_session est manquante";
        static final String MSG_NOM_SESSION_VIDE = "La propriete nom_session est une chaine vide";
        static final String MSG_TYPE_SESSION_MANQUANTE = "La propriete type_session est manquante";
        static final String MSG_RECOMPENSE_ENFANTS_MANQUANTE = "La propriete recompenpse_enfants est manquante";
        static final String MSG_RECOMPENSE_PARENTS_MANQUANTE = "La propriete recompense_parents est manquante";
        static final String MSG_FAMILLES_MANQUANT = "La propriete familles est manquante";
        static final String MSG_NOM_FAMILLE_MANQUANT = "La propriete nom est manquante";
        static final String MSG_NOM_FAMILLE_VIDE = "La valeur du nom ne doit pas etre une chaine vide.";
        static final String MSG_NOM_FAMILLE_EXISTANT = "La valeur du champ nom d'une famille doit etre unique.";
        static final String MSG_TOTAL_TACHES_ENFANTS_MANQUANT = "La propriete total_taches_enfants est manquante";
        static final String MSG_TOTAL_TACHES_PARENTS_MANQUANT = "La propriete total_taches_parents est manquante";
        static final String MSG_DATE_INSCRIPTION_MANQUANTE = "La propriete date_inscription est manquante";


        private JSONObject typeSessionJson;
        public static JSONObject resultat;
        private String donneeJSON;


        public MappeurDonneesJSON(String donneeJSON, JSONObject resultat) {
            MappeurDonneesJSON.resultat = resultat;
            this.donneeJSON = donneeJSON;
        }

        public TypeSession ObtenirTypeSessionAPartirDeJson() {
            typeSessionJson = JSONObject.fromObject(donneeJSON);
            TypeSession typesession = CreerTypeSession(resultat);
            CreerFamilles(typesession);
            return typesession;
        }

        public TypeSession CreerTypeSession(JSONObject resultat) {
            TypeSession session = new TypeSession();

            try {
                String nomSession = typeSessionJson.getString("nom_type_session");
                session.setNomSession(nomSession);
            } catch (ValidationDonneesException ex) {
                ecrireMessageErreurEtArreteLAppli(resultat, MSG_NOM_SESSION_VIDE);
            } catch (Exception e) {
                ecrireMessageErreurEtArreteLAppli(resultat, MSG_NOM_SESSION_MANQUANTE);
            }

            int typeSession;

            try {
                typeSession = typeSessionJson.getInt("type_session");
                session.setTypeSession(typeSession);

            } catch (ValidationDonneesException ex) {
                ecrireMessageErreurEtArreteLAppli(resultat, MSG_TYPE_SESSION);

            }

            return session;
        }

        private void ecrireMessageErreurEtArreteLAppli(JSONObject resultat, String erreurMessageText) {
            resultat.accumulate("message", erreurMessageText);

            System.exit(0);
        }

        private void CreerFamilles(TypeSession session) {
            JSONArray familles = new JSONArray();
            int cptParents=0;
            int cptEnfants=0;
            int cptTachesEnfants=0;
            int cptTachesParents=0;

            familles = typeSessionJson.getJSONArray("familles");
            int nbFamilles = familles.size();

            if (nbFamilles == 0) {
                resultat.accumulate("message", MSG_NOMBRE_FAMILLES_MINIMUM);

                System.exit(0);
            }
            for (int i = 0; i < nbFamilles; i++) {
                try {
                    JSONObject familleJson = familles.getJSONObject(i);
                    Famille famille = new Famille();

                    try {
                        String nom_famille = familleJson.getString("nom_famille");
                        famille.setNomFamille(nom_famille);
                    } catch (ValidationDonneesException ex) {
                        ecrireMessageErreurEtArreteLAppli(resultat, MSG_NOM_FAMILLE_VIDE);

                    }


                    try {
                        int totalTachesEnfants = familleJson.getInt("total_taches_enfants");
                        famille.setTotalTachesEnfants(totalTachesEnfants);
                        cptTachesEnfants= totalTachesEnfants + cptTachesEnfants;
                        session.setTotalTachesEnfants(cptTachesEnfants);
                    } catch (ValidationDonneesException ex) {
                        ecrireMessageErreurEtArreteLAppli(resultat, MSG_TOTAL_TACHES_ENFANTS_MANQUANT);
                    }

                    try {
                        int totalTachesParents = familleJson.getInt("total_taches_parents");
                        famille.setTotalTachesParents(totalTachesParents);
                        cptTachesParents = totalTachesParents + cptTachesParents;
                        session.setTotalTachesParents(cptTachesParents);

                    } catch (ValidationDonneesException ex) {
                        ecrireMessageErreurEtArreteLAppli(resultat, MSG_TOTAL_TACHES_PARENTS_MANQUANT);
                    }

                    try {
                        int nbEnfants = familleJson.getInt("nombre_enfants");
                        famille.setnbEnfants(nbEnfants);
                        cptEnfants = cptEnfants + nbEnfants;
                        session.setTotalEnfants(cptEnfants);
                    } catch (ValidationDonneesException ex) {
                        ecrireMessageErreurEtArreteLAppli(resultat, MSG_NOMBRE_ENFANTS);

                    }

                    try {
                        int nbParents = familleJson.getInt("nombre_parents");
                        famille.setnbParents(nbParents);
                        cptParents = cptParents + nbParents;
                        session.setTotalParents(cptParents);
                    } catch (ValidationDonneesException ex) {
                        ecrireMessageErreurEtArreteLAppli(resultat, MSG_NOMBRE_PARENTS);

                    }

                    try {
                        int nbEnfants = familleJson.getInt("recompense_enfants");
                        famille.setRecompenseEnfants(nbEnfants);
                    } catch (ValidationDonneesException ex) {
                        ecrireMessageErreurEtArreteLAppli(resultat, MSG_NOMBRE_ENFANTS);

                    }

                    try {
                        int nbParents = familleJson.getInt("recompense_parents");
                        famille.setRecompenseParents(nbParents);
                    } catch (ValidationDonneesException ex) {
                        ecrireMessageErreurEtArreteLAppli(resultat, MSG_NOMBRE_PARENTS);

                    }


                    try {
                        String date_insc = familleJson.getString("date_inscription");
                        famille.setDateInscription(date_insc);
                    } catch (ValidationDonneesException ex) {
                        ecrireMessageErreurEtArreteLAppli(resultat, MSG_FORMAT_DATE);

                    }

                    session.ajouterFamille(famille);
                } catch (ValidationDonneesException ex) {
                    ecrireMessageErreurEtArreteLAppli(resultat, MSG_NOM_FAMILLE_EXISTANT);

                }
            }

        }

}
