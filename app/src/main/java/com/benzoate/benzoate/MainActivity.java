package com.benzoate.benzoate;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.benzoate.benzoate.BackGroudUtility.DataBaseHelper;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;


public class MainActivity extends AppCompatActivity {

    String mobileAdsNumber = "ca-app-pub-3940256099942544~3347511713";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeAds();
        DataBaseHelper.CreateDataBase(this);
    }

    private void initializeAds() {
        MobileAds.initialize(this, mobileAdsNumber);
        AdView mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    public void toastMessage(String message) {
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, message, duration);
        toast.show();

    }

    public void onButtonClick(View v) {

        if (v.getId() == R.id.button_login) {

            EditText a = findViewById(R.id.editText_username);
            String str = a.getText().toString();
            String pass = ((EditText) findViewById(R.id.editText_password)).getText().toString();

            String password = DataBaseHelper.myDb.searchPassword(str);
            if (pass.equals(password)) {
                Intent i = new Intent(MainActivity.this, Dashboard.class);
                i.putExtra("Username", str);
                startActivity(i);
            } else {
                Toast.makeText(MainActivity.this, "Username and Password don't match!", Toast.LENGTH_LONG).show();
            }
        }

        if (v.getId() == R.id.button_signup) {
            Intent i = new Intent(MainActivity.this, SignUp.class);
            startActivity(i);
        }

    }
}
